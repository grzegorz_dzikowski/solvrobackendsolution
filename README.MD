# Solvro Bus System
Author: Grzegorz Dzikowski

Backend functionalities of the Bus System

# Prerequests
Maven with Java 1.8 minumum
# Quick Start
1. Put the directory of the stops file and desired users json location in application.conf in resources folder
2. In main directory, execute code with command `mvn kotlin:compile exec:java`

# Documentation
Authorisation is performed using Basic Auth
You need to register the user using RESTER and /register api
[Here](https://github.com/Solvro/rekrutacja/blob/master/backend/stops_api.yaml)
Beside this:
POST /register:
    parameters: String email, String username, String password
    description: Registers new user to the system
    response:
        BAD_REQUEST: If data is invalid or user already exists
        OK: If registration is sucessfull

# Configuration:
You can change port and paths in application.conf

# About:
Program rekrutacyjny do KN Solvro. Program wymaga wiele ulepszeń, ale wstępnie działa. Testowane na Windows 10 na JDK 12
