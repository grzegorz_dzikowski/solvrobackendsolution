package org.zgrate.solvro

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonView


/**
 * Entity for  map of the stops, in nx format
 * @param directed - is the map directed or undirected
 * @param graph - map of the graph
 * @param
 */
data class CityMap(
    var links: List<Link> = emptyList(),
    var stops: List<StopNode> = emptyList()
)

/**
 * Stores StopData
 * @param id Id of the stop
 * @param name Name of the stop
 */
data class StopNode(
    @JsonView(Views.Internal::class)
    @JsonProperty("id")
    var id: Int = 0,

    @JsonProperty("name")
    @JsonAlias("stop_name")
    @JsonView(Views.Public::class)
    var stopName: String = ""
)

/**
 * Link between the two stops
 * @param source Id of the source stop
 * @param target Id of the target stop
 * @param distance Distance between stops
 */
data class Link(var source: Int = -1, var target: Int = -1, var distance: Int = 0)

/**
 * User data
 * @param id Random user ID
 * @param username Username of the user
 * @param hash Salted password of the user
 * @param email Email of the user
 */
data class User(val id: Int = 0, val username: String = "", val hash: String = "", val email: String = "")


/**
 * Views class, used to show correct data in serialization
 */
class Views {
    open class Public

    class Internal : Public()
}

/**
 * Calculated path between two nodes
 * @param distance Distance between them
 * @param stops list of Stops between
 */
data class CalculatedPath(val distance: Int = 0, val stops: List<StopNode> = listOf())
