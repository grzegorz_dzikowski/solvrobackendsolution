package org.zgrate.solvro

import com.fasterxml.jackson.databind.ObjectMapper
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.util.*

/**
 * Main Object Mapper, used to read and save data to and from Jsons
 */
val objectMapper = ObjectMapper()
/**
 * Generator for random user ids
 */
val randomGenerator = Random()


class MapServiceImpl(val repository: MapRepository) : MapService {
    override fun getStop(name: String): StopNode? {
        return repository.getCityMap().stops.find { it.stopName == name }
    }

    override fun getLink(source: StopNode, target: StopNode): Link? {
        return repository.getLinksFromStop(source, true).find { it.target == target.id }
    }

    override fun getShortestPath(source: StopNode, target: StopNode): CalculatedPath {
        val values = mutableListOf<StopNode>()
        val distance = findShortestRoute(source, target, values)
        if (distance == Integer.MAX_VALUE) {
            values.clear()
            val dis = findShortestRoute(target, source, values, true)
            return CalculatedPath(dis, values.also { it.add(target) })
        }
        return CalculatedPath(distance, values.also { it.add(source) }.reversed())
    }

    /**
     * TODO: Not tested if source and target are non-connected with any system, will system handle this situation?'
     * TODO: Not really tested if back tracking the stops works (reverse links)
     */
    private fun findShortestRoute(
        source: StopNode,
        target: StopNode,
        list: MutableList<StopNode>,
        back: Boolean = false
    ): Int {
        if (target == source) {
            return 0
        }
        var shortest = Int.MAX_VALUE
        var curr = list
        repository.getLinksFromStop(source, back).forEach { link ->
            val copy = mutableListOf<StopNode>()
            val dist = findShortestRoute(getStop(link.target)!!, target, copy)
            if (dist != Int.MAX_VALUE && dist + link.distance < shortest) {
                curr = copy.also { it.add(getStop(link.target)!!) }
                shortest = dist + link.distance
            }
        }
        list.addAll(curr)
        return shortest
    }

    override fun getStops(): List<StopNode> {
        return repository.getCityMap().stops
    }


    override fun getStop(id: Int): StopNode? {
        return repository.getCityMap().stops.find { it.id == id }
    }
}

/**
 * Conctruct the MapRepositoryImpl
 * @param path Path for JSON file in networkX format for loading the stops
 */
class MapRepositoryImpl(private val path: String) : MapRepository {
    override fun getLinksFromStop(source: StopNode, onlyBack: Boolean): List<Link> {
        return if (!onlyBack) {
            mapCity.links.filter { it.source == source.id }
        } else {
            //val a = mapCity.links.filter { it.source == source.id }.toMutableList()
            mapCity.links.filter { it.target == source.id }
                .map { Link(source = it.target, target = it.source, distance = it.distance) }
            //b.forEach{ a.add(Link(it.target, it.source, it.distance))}

        }

    }


    override fun getCityMap(): CityMap {
        return mapCity
    }

    private val mapCity = CityMap()

    private fun loadJSON(json: File) {
        val noded = objectMapper.readTree(json)
        mapCity.stops = objectMapper.readValue<List<StopNode>>(
            noded["nodes"].traverse(),
            objectMapper.typeFactory.constructCollectionType(List::class.java, StopNode::class.java)
        ).distinctBy { it.stopName }
        mapCity.links = objectMapper.readValue<List<Link>>(
            noded["links"].traverse(),
            objectMapper.typeFactory.constructCollectionType(List::class.java, Link::class.java)
        ).filter { link -> mapCity.stops.any { it.id == link.source } && mapCity.stops.any { it.id == link.target } }
    }

    init {
        loadJSON(File(path))
    }
}

/**
 * @param path Path for a JSON file for loading the user data
 */
class UserRepositoryImpl(private val path: String) : UserRepository {

    private val users: MutableList<User>

    init {
        if (File(path).exists()) {
            try {
                users = objectMapper.readValue<MutableList<User>>(
                    File(path),
                    objectMapper.typeFactory.constructCollectionType(MutableList::class.java, User::class.java)
                )
            } catch (ex: Exception) {
                throw RuntimeException("An error occured during loading the file", ex)
            }
        } else {
            users = mutableListOf()
            saveFile()
        }
    }

    private fun saveFile(): Boolean {
        return try {
            objectMapper.writeValue(File(path), users)
            true
        } catch (ex: Exception) {
            false
        }
    }

    override fun getUser(name: String): User? {
        return users.find { it.username == name }
    }

    override fun getUserByMail(email: String): User? {
        return users.find { it.email == email }
    }

    override fun registerUser(user: User): Boolean {
        users.add(user)
        return saveFile()
    }

}

class UserServiceImpl(private var userRepository: UserRepository) : UserService {

    private fun unhashPassword(pass: String, hash: String): Boolean {
        return BCrypt.checkpw(pass, hash)
    }

    private fun hashPassword(pass: String): String {
        return BCrypt.hashpw(pass, BCrypt.gensalt())
    }


    override fun checkPassword(user: String, password: String): Boolean {
        return unhashPassword(password, userRepository.getUser(user)?.hash ?: return false)
    }

    override fun registerUser(user: String, password: String, email: String): Boolean {
        if (userRepository.getUser(user) != null || userRepository.getUserByMail(email) != null) {
            return false
        }
        if (!email.matches(Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))) {
            return false
        }
        return userRepository.registerUser(
            User(
                id = randomGenerator.nextInt(), username = user,
                hash = hashPassword(password), email = email
            )
        )
    }

}