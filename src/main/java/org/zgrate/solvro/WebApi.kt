package org.zgrate.solvro

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.authenticate
import io.ktor.auth.basic
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.util.getOrFail
import org.koin.dsl.module
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject

/**
 * Extension class for registering in Ktor and for initializing the Kion
 */
fun Application.main() {
    //Default loggers, headers processor and content response manager
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation)

    //Initializator of all modules, present in Kion, prepared for dependency injection
    val modules = module {
        single<MapService> { MapServiceImpl(get()) } // get() Will resolve HelloRepository
        single<MapRepository>(createdAtStart = true) {
            MapRepositoryImpl(
                this@main.environment.config.propertyOrNull("ktor.configs.stops")?.getString() ?: "stops.json"
            )
        }
        single<UserService> { UserServiceImpl(get()) }
        single<UserRepository>(createdAtStart = true) {
            UserRepositoryImpl(
                this@main.environment.config.propertyOrNull("ktor.configs.usersDb")?.getString() ?: "users.json"
            )
        }
    }

    //Initializes Kion with modules
    install(Koin) {

        modules(modules)
    }

    //Initializes exception handlers
    install(StatusPages) {
        exception<Throwable> { e ->
            e.printStackTrace() //TODO: Change this to some logger
            call.respondText(e.localizedMessage, ContentType.Text.Plain, HttpStatusCode.InternalServerError)
        }
    }

    val userService: UserService by inject()
    val mapService: MapService by inject()

    //Authorization Module
    install(Authentication) {
        basic(name = "basic_auth") {
            realm = "Solvro Route System" //TODO: Fancy name
            validate { credit ->
                if (userService.checkPassword(credit.name, credit.password)) {
                    UserIdPrincipal(credit.name)
                } else {
                    null
                }
            }
        }
    }

    //Actual routing for the system
    routing {
        authenticate("basic_auth") {
            get("/stops")
            {
                call.respondText(contentType = ContentType.parse("application/json"), provider = suspend {
                    objectMapper.writerWithView(Views.Public::class.java).writeValueAsString(mapService.getStops())
                })
            }

            get("/path")
            {
                val para = call.request.queryParameters
                val source = mapService.getStop(para.getOrFail("source"))
                val target = mapService.getStop(para.getOrFail("target"))
                when {
                    source == null -> call.respondText(status = HttpStatusCode.BadRequest, text = "source_not_found")
                    target == null -> call.respondText(status = HttpStatusCode.BadRequest, text = "target_not_found")
                    else -> call.respondText(contentType = ContentType.parse("application/json"), provider = suspend {
                        objectMapper.writerWithView(Views.Public::class.java)
                            .writeValueAsString(mapService.getShortestPath(source, target))
                    })
                }

            }

        }

        post("/register")
        {
            val c = call.receive<Parameters>()
            if (userService.registerUser(c.getOrFail("email"), c.getOrFail("password"), c.getOrFail("username"))) {
                call.respond(HttpStatusCode.OK, "")
            } else {
                call.respond(HttpStatusCode.BadRequest, "user_create_error")
            }
        }

    }
}

