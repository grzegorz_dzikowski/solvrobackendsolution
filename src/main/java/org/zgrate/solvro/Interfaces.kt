package org.zgrate.solvro

/**
 * Map Service interface, used as a main provider for Map and stops
 */
interface MapService {
    /**
     * Get Stop by it's ID
     * @param id - id of the stop
     * @return StopNode associated with the id
     */
    fun getStop(id: Int): StopNode?

    /**
     * Gets stops by it's name
     * @param name Name of the stop
     * @return Stop with given name, otherwise null
     */
    fun getStop(name: String): StopNode?

    /**
     * Get all stops
     * @return List of all stoops
     */
    fun getStops(): List<StopNode>

    /**
     * Calculates the shortest path from source to target
     * @param source Start of the path
     * @param target End of the path
     * @return Calculated Path with processed path
     */
    fun getShortestPath(source: StopNode, target: StopNode): CalculatedPath

    /**
     * Gets direct link between given points
     * @param source Start node
     * @param target Target node
     * @return if the link exists, returns Link between this nodes, else it returns null
     */
    fun getLink(source: StopNode, target: StopNode): Link?
}

/**
 * Map Data Repository Inferface
 */
interface MapRepository {
    /**
     * Gets saved City Map
     * @return City Map from Json File
     */
    fun getCityMap(): CityMap

    /**
     * Finds and returns all links, which target is source
     * @param source Starting point of all nodes
     * @param onlyBack Find only sources as targets and reverse them (reverse link)
     * @return List of all links with target in source
     */
    fun getLinksFromStop(source: StopNode, onlyBack: Boolean): List<Link>
}

/**
 * User Service Interfaces, used to manage users
 */
interface UserService {
    /**
     * Authentifzicate the user with given password
     * @param user Username of the requested user
     * @param password Plantext password to check the password
     * @return True if login is sucessfull, otherwise false
     */
    fun checkPassword(user: String, password: String): Boolean

    /**
     * Registers new user in ssytem
     * @param user Username to register user with
     * @param password = Password of the user
     * @param email Email of user
     * @return True if user is sucessfully registered, otherwise false
     */
    fun registerUser(user: String, password: String, email: String): Boolean
}

/**
 * Reposetory for accessing user data
 */
interface UserRepository {
    /**
     * Gets the user by the username
     * @param name Username to request
     * @return User by the given Username, null if not found
     */
    fun getUser(name: String): User?

    /**
     * Gets the user by the email
     * @param email Email of the user
     * @return User if email exists, otherwise null
     */
    fun getUserByMail(email: String): User?

    /**
     * Registers user in system
     * @param user User to register
     * @return True if sucessfull, otherwise falses
     */
    fun registerUser(user: User): Boolean
}
